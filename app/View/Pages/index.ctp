<!-- main section -->
        <section id="main-section" class="main-section parallax-header version-1">
            <div class="bg-effect parallax">
                <img class="parallax-img" src="images/main-bg-1.jpg" alt="" />
                
            </div>
            <svg class="main-block-svg" x="0px" y="0px" viewBox="-1068.8 -253 1940.7 801.9" enable-background="new -1068.8 -253 1940.7 801.9">
                <polyline fill="none" stroke="#FFFFFF" stroke-width="3" stroke-miterlimit="10" points="-1068.8,548.9 -854.9,438.9 -542.9,504.9 
                45,223 153,269 871.9,-253 "/>
            </svg>
            <div class="container">
                <div class="head-title-block main-section-title">
                    <h1>
                        WE ARE PROFI
                    </h1>
                    <h3>QUALITY. UNIQUENESS. SIMPLICITY.</h3>
                    <a class="btn btn-primary reserve-popup">Contact us</a>
                </div>
                <div class="slide-bottom">
                    <span class="fa fa-angle-down"></span>
                </div>
            </div>
        </section>
        <!-- main section end -->

        <!-- info block -->
        <section id="info-block" class="info-block version-1">
            <h2 class="section-title">SERVICES</h2>
            <p class="section-description">Our every work is full of great ideas and nice solutions</p>
            <div class="container">
                <div class="row">
                    <div class="info-block-inner">
                        <div class="col-xs-12 col-sm-6 col-md-3 info-block-col">
                            <div class="info-block-img">
                                <img src="images/services-1-index-1.jpg" alt="" />
                                <i class="fa fa-leaf"></i>
                            </div>
                            <h5>Business Formations</h5>
                            <p>Maintain compliance with the firm that invented the registered.</p>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 info-block-col">
                            <div class="info-block-img">
                                <img src="images/services-2-index-1.jpg" alt="" />
                                <i class="fa fa-eur "></i>
                            </div>
                            <h5>Deal Services</h5>
                            <p>Handle corporate transitions with simple, straight-forward ease.</p>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 info-block-col">
                            <div class="info-block-img">
                                <img src="images/services-3-index-1.jpg" alt="" />
                                <i class="fa fa-flag"></i>
                            </div>
                            <h5>Business Process</h5>
                            <p>Establish a solid and successful foundation for your business.</p>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 info-block-col">
                            <div class="info-block-img">
                                <img src="images/services-4-index-1.jpg" alt="" />
                                <i class="fa fa-plane"></i>
                            </div>
                            <h5>International</h5>
                            <p>Take the first step toward establishing your global footprint.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- info block end -->
        
        <!-- Team  -->
        <section id="team" class="team version-1">
            <div class="bg-effect image" style="background-image: url('./images/main-bg-1.jpg');">
            </div>
            <h2 class="section-title">Our Team</h2>
            <p class="section-description">Like a big Family</p>
            <div class="container">
                <div class="row">
                    <div class="team-carousel">
                        <article class="team-lazy col-xs-6 col-sm-4 col-md-3">
                            <div class="team-body">
                                <div class="image-block">
                                    <img src="images/team-1-index-4.jpg" alt="" />
                                </div>
                                <div class="team-description">
                                    <h5 class="title">Lucy Watson</h5>
                                    <p class="office">CMO</p>
                                    <p class="about">Let's built the coolest brands and products</p>
                                    <ul class="social-links">
                                        <li><a class="fa fa-youtube-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-facebook-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-twitter-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-google-plus-square" href="wsdindex.html#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                        <article class="team-lazy col-xs-6 col-sm-4 col-md-3">
                            <div class="team-body">
                                <div class="image-block">
                                    <img src="images/team-2-index-4.jpg" alt="" />
                                </div>
                                <div class="team-description">
                                    <h5 class="title">Andy Moore</h5>
                                    <p class="office">CTO, Co-founder</p>
                                    <p class="about">Let's solve complex challenges by a single </p>
                                    <ul class="social-links">
                                        <li><a class="fa fa-youtube-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-facebook-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-twitter-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-google-plus-square" href="wsdindex.html#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                        <article class="team-lazy col-xs-6 col-sm-4 col-md-3">
                            <div class="team-body">
                                <div class="image-block">
                                    <img src="images/team-3-index-4.jpg" alt="" />
                                </div>
                                <div class="team-description">
                                    <h5 class="title">Lucia Doe</h5>
                                    <p class="office">Team Lead</p>
                                    <p class="about">We have built a serious team with accurate approach</p>
                                    <ul class="social-links">
                                        <li><a class="fa fa-youtube-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-facebook-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-twitter-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-google-plus-square" href="wsdindex.html#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                        <article class="team-lazy col-xs-6 col-sm-4 col-md-3">
                            <div class="team-body">
                                <div class="image-block">
                                    <img src="images/team-4-index-4.jpg" alt="" />
                                </div>
                                <div class="team-description">
                                    <h5 class="title">John Doe</h5>
                                    <p class="office">Designer, Co-founder</p>
                                    <p class="about">Our strengths are pure and ergonomic design</p>
                                    <ul class="social-links">
                                        <li><a class="fa fa-youtube-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-facebook-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-twitter-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-google-plus-square" href="wsdindex.html#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                        <article class="team-lazy col-xs-6 col-sm-4 col-md-3">
                            <div class="team-body">
                                <div class="image-block">
                                    <img src="images/team-5-index-4.jpg" alt="" />
                                </div>
                                <div class="team-description">
                                    <h5 class="title">Michael Smith</h5>
                                    <p class="office">CEO, Co-founder</p>
                                    <p class="about">Our service resolves important and significant issues</p>
                                    <ul class="social-links">
                                        <li><a class="fa fa-youtube-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-facebook-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-twitter-square" href="wsdindex.html#"></a></li>
                                        <li><a class="fa fa-google-plus-square" href="wsdindex.html#"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <!-- Team end -->
        
        <!-- products -->
        <section id="products" class="products version-1">
            <h2 class="section-title">Products</h2>
            <p class="section-description">Our main products</p>
            <div class="products-body">
                <div class="container">
                    <div class="row">
                        <div class="products-list">
                            <article>
                                <div class="col-xs-12 col-sm-6 prod-image">
                                    <img src="images/prod-1.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 prod-description">
                                    <h5>Social Polls</h5>
                                    <p class="line-lg"><span></span></p>
                                    <p>Our company conducts mass social surveys and researches. Ownership certificates, sample documents, plus a metal seal. Reserve your business name so it'll be available when needed And offers results with analytics for your productivity work.</p>
                                    <a class="btn btn-success btn-sm" href="product_page.html">Read more</a>
                                </div>
                            </article>
                            <article>
                                <div class="col-xs-12 col-sm-6 prod-image">
                                    <img src="images/prod-2.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 prod-description">
                                    <h5>Non-profit</h5>
                                    <p class="line-lg"><span></span></p>
                                    <p>Promote charitable, educational or other public causes. It creates positive image of your company. and strengthens its social status. Ownership certificates, sample documents, plus a metal seal. Reserve your business name so it'll be available when needed</p>
                                    <a class="btn btn-success btn-sm" href="product_page.html">Read more</a>
                                </div>
                            </article>
                            <article>
                                <div class="col-xs-12 col-sm-6 prod-image">
                                    <img src="images/prod-3.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 prod-description">
                                    <h5>Corporation Services</h5>
                                    <p class="line-lg"><span></span></p>
                                    <p>Ownership certificates, sample documents, plus a metal seal. Reserve your business name so it'll be available when needed. Learn how to do good and efficient while making a profit. How to raise efficiency, productivity and to increase KPI your company.</p>
                                    <a class="btn btn-success btn-sm" href="product_page.html">Read more</a>
                                </div>
                            </article>
                            <article>
                                <div class="col-xs-12 col-sm-6 prod-image">
                                    <img src="images/prod-4.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 prod-description">
                                    <h5>Compliance Kit</h5>
                                    <p class="line-lg"><span></span></p>
                                    <p>Customized record book, ownership certificates, sample documents, plus a metal seal. Customized record book, ownership certificates, sample documents, plus a metal seal. Reserve your business name so it'll be available when needed.</p>
                                    <a class="btn btn-success btn-sm" href="product_page.html">Read more</a>
                                </div>  
                            </article>
                        </div>
                    </div>
                </div>
                <a class="next fa fa-angle-right" href="wsdindex.html#"></a>
                <a class="prev fa fa-angle-left" href="wsdindex.html#"></a>
            </div>
            <ul class="products-thumbs">
                <li class="active"><img src="images/prod-1.jpg" alt="" /></li>
                <li><img src="images/prod-2.jpg" alt="" /></li>
                <li><img src="images/prod-3.jpg" alt="" /></li>
                <li><img src="images/prod-4.jpg" alt="" /></li>
            </ul>
        </section>
        <!-- products end -->

        <!-- pricing -->
        <section id="pricing-block" class="pricing-block version-1">
            <div class="bg-effect image" style="background-image: url('./images/main-bg-1.jpg');">
            </div>
            <h2 class="section-title">Pricing</h2>
            <p class="section-description">Don't miss something special!</p>
            <div class="container">
                <ul class="pricing-list">
                    <li class="options-list">
                        <header> 
                        </header>
                        <ul class="pricing-inner-list">
                            <li>No limits on time or users</li>
                            <li>Per user per month</li>
                            <li>20 GB free space on server</li>
                            <li>Unlim free space on server</li>
                            <li>Special support chat</li>
                            <li>24/7 technical support</li>
                        </ul>
                    </li>
                    <li>
                        <header>
                            <h5>FREE</h5>
                            <p class="price-value"><i>$</i>0</p>
                        </header>
                        <ul class="pricing-inner-list">
                            <li>+</li>
                            <li>+</li>
                            <li><i class="point"></i></li>
                            <li><i class="point"></i></li>
                            <li><i class="point"></i></li>
                            <li><i class="point"></i></li>
                        </ul>
                        <a class="btn btn-primary btn-sm" href="product_page.html">free</a>
                    </li>
                    <li class="popular">
                        <header>
                            <h5>PRO <span>best</span></h5>
                            <p class="price-value"><i>$</i>30</p>
                        </header>
                        <ul class="pricing-inner-list">
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                            <li><i class="point"></i></li>
                            <li><i class="point"></i></li>
                        </ul>
                        <a class="btn btn-primary btn-sm inverse-btn" href="product_page.html">buy</a>
                    </li>
                    <li>
                        <header>
                            <h5>ENTERPRISE</h5>
                            <p class="price-value"><i>$</i>100</p>
                        </header>
                        <ul class="pricing-inner-list">
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                            <li>+</li>
                        </ul>
                        <a class="btn btn-primary btn-sm" href="product_page.html">buy</a>
                    </li>
                </ul>
            </div>
        </section>
        <!-- pricing end -->

        <!-- Counter -->
        <section id="counter-block" class="counter-block version-2">
            <h2 class="section-title">Counter</h2>
            <p class="section-description">Our statistic</p>
            <div class="container">
                <ul class="counter-list">
                    <li>
                        <div>
                            <svg x="0px" y="0px" viewBox="0 0 49.1 46.2" enable-background="new 0 0 49.1 46.2">
                                <path fill="#FFFFFF" d="M37.5,18.4v1.4h-5.1c-1.1,0-1.4-0.4-1.7-0.9c-0.9-1.7-1.8-5.4-2.8-9.6c-0.3-1.1-0.6-2.3-0.9-3.5
                                c-0.1-0.6-0.6-2.4-2.6-2.4c-1.9,0-4.3,2.7-4.3,5.9v6.1c0,1.4-1.1,2.5-2.5,2.5h-7.1c-2.9,0-4.9,1.4-5.2,3.6c-0.1,1.2,0.3,2.4,1.1,3.3
                                c-0.7,0.8-1.1,1.7-1.1,2.8c0,1,0.4,2,1.1,2.8c-0.7,0.8-1.1,1.7-1.1,2.8c0,1,0.4,2,1.1,2.8c-0.8,0.9-1.2,2.1-1.1,3.3
                                c0.3,2,2.1,3.5,4.2,3.5h18.1c1.3,0,2.4-0.7,3.3-1.3c0.8-0.5,1.6-1,2.5-1h4v2.4h6.4V18.4H37.5z M30.4,40.6c-0.9,0.6-1.8,1.1-2.8,1.1
                                H9.5c-1.6,0-3-1.1-3.2-2.7C6.1,38,6.6,37,7.4,36.3c0.1-0.1,0.2-0.3,0.2-0.4s-0.1-0.3-0.2-0.4c-0.7-0.6-1.1-1.5-1.1-2.4
                                c0-0.9,0.4-1.8,1.1-2.4c0.1-0.1,0.2-0.3,0.2-0.4S7.5,30,7.4,29.9c-0.7-0.6-1.1-1.5-1.1-2.4s0.4-1.8,1.1-2.4c0.1-0.1,0.2-0.3,0.2-0.4
                                s-0.1-0.3-0.2-0.4c-0.8-0.7-1.2-1.7-1.1-2.7c0.2-1.7,1.8-2.7,4.2-2.7h7.1c1.9,0,3.5-1.6,3.5-3.5V9.2c0-2.7,2-4.9,3.3-4.9
                                c0.9,0,1.3,0.5,1.6,1.6c0.3,1.2,0.6,2.4,0.9,3.5c1.1,4.3,2,8,2.9,9.8c0.5,1,1.3,1.4,2.6,1.4h2.4v18.6h-1.4
                                C32.2,39.4,31.3,40,30.4,40.6z M37,39.4h-1.2V20.8H37V39.4z"/>
                            </svg>
                            <b class="counter">100</b>
                            <i>%</i>
                        </div>
                        <p>POSITIVE FEEDBACK</p>
                    </li>
                    <li>
                        <div>
                            <svg x="0px" y="0px" viewBox="0 0 49.1 46.2" enable-background="new 0 0 49.1 46.2">
                                <path fill="#FFFFFF" d="M24.5,38.9C15.4,38.9,8,31.4,8,22.3S15.4,5.7,24.5,5.7s16.6,7.4,16.6,16.6c0,3.6-1.1,7.1-3.3,10
                                    c-0.2,0.2-0.1,0.5,0.1,0.7c0.2,0.2,0.5,0.1,0.7-0.1c2.3-3.1,3.5-6.7,3.5-10.6c0-9.7-7.9-17.6-17.6-17.6C14.9,4.7,7,12.6,7,22.3
                                    s7.9,17.6,17.6,17.6c0.3,0,0.5-0.2,0.5-0.5S24.8,38.9,24.5,38.9z"/>
                                <circle fill="#FFFFFF" cx="33.2" cy="29.4" r="3.9"/>
                                <path fill="#FFFFFF" d="M36.3,34.6h-6c-2.1,0-3.9,1.7-3.9,3.9v3h13.8v-3C40.2,36.3,38.4,34.6,36.3,34.6z"/>
                                <path fill="#FFFFFF" d="M18.3,19.3c-1.2,0.4-2.3,0.8-3.3,1.1c-2.5,0.9-4,3.6-3.3,6.2c1.3,5,4.3,7.6,6.6,8.8
                                    c0.4,0.2,0.7,0.3,1.1,0.3c0.6,0,1.3-0.3,1.7-0.7c0.4-0.5,0.7-1.1,0.6-1.7c-0.1-1.8,0.5-3.6,1.7-5.4c1.4-2.2,1.7-4.5,0.7-6.4
                                    C23.1,19.5,20.6,18.6,18.3,19.3z M22.6,27.3c-1.3,2-1.9,4-1.8,6c0,0.5-0.2,0.8-0.4,1c-0.4,0.4-1.1,0.6-1.7,0.3
                                    c-2.1-1.2-4.8-3.5-6.1-8.2c-0.5-2,0.6-4.2,2.7-5c1-0.3,2.1-0.7,3.3-1.1c0.4-0.1,0.9-0.2,1.3-0.2c1.4,0,2.6,0.7,3.3,1.9
                                    C24.1,23.5,23.9,25.4,22.6,27.3z"/>
                                <path fill="#FFFFFF" d="M38.9,20.2c0.6,0,1.1-0.2,1.6-0.5c0.2-0.2,0.3-0.5,0.1-0.7s-0.5-0.3-0.7-0.1c-0.2,0.2-0.7,0.4-1.4,0.3
                                    c-0.5-0.1-1.4-0.6-2.1-2c-2.2-4.1-9.2-2.2-13.3-1c-0.4,0.1-0.8,0.2-1.2,0.3c-1.9,0.5-3.1,0.5-3.6,0c-0.5-0.5-0.2-1.7,0.1-2.5
                                    c0.3-0.8,0.2-1.5-0.2-2.2c-0.8-1.2-2.8-1.7-4.2-1.7c-0.2,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5c1.3,0,2.8,0.5,3.4,1.3
                                    c0.2,0.4,0.3,0.8,0.1,1.3c-0.6,1.6-0.6,2.8,0.1,3.6c0.8,0.9,2.3,1,4.6,0.3c0.4-0.1,0.8-0.2,1.2-0.3c3.9-1.1,10.3-2.9,12.2,0.5
                                    c0.9,1.8,2,2.3,2.8,2.5C38.5,20.2,38.7,20.2,38.9,20.2z"/>
                            </svg>
                            <b class="counter">456</b>
                            <i>k</i>
                        </div>
                        <p>USERS AROUND WORLD</p>
                    </li>
                    <li>
                        <div>
                            <svg x="0px" y="0px" viewBox="0 0 49.1 46.2" enable-background="new 0 0 49.1 46.2">
                                <path fill="#FFFFFF" d="M38.6,5.3H10.5c-0.3,0-0.5,0.2-0.5,0.5v34.6c0,0.3,0.2,0.5,0.5,0.5h28.2c0.3,0,0.5-0.2,0.5-0.5V5.8
                                    C39.1,5.5,38.9,5.3,38.6,5.3z M11,6.3h27.2V34h-5.4c-0.3,0-0.5,0.2-0.5,0.5v5.4H11V6.3z M37.4,35l-4.2,4.2V35H37.4z M34,39.9
                                    l4.2-4.2v4.2H34z"/>
                                <path fill="#FFFFFF" d="M37.3,11.2c0-0.3-0.2-0.5-0.5-0.5H12.3c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h24.4
                                    C37,11.7,37.3,11.5,37.3,11.2z"/>
                                <polygon fill="#FFFFFF" points="31.7,20.6 27.8,20.6 27.8,15.2 21.3,15.2 21.3,20.6 17.4,20.6 24.5,28.1   "/>
                                <rect x="18.1" y="29.5" fill="#FFFFFF" width="12.8" height="2.4"/>
                            </svg>
                            <b class="counter">500</b>
                        </div>
                        <p>DOWNLOADS PER DAY</p>
                    </li>
                    <li>
                        <div>
                            <svg x="0px" y="0px" viewBox="0 0 49.1 46.2" enable-background="new 0 0 49.1 46.2">
                                <path fill="#FFFFFF" d="M19.4,12h-0.7v2.4c0.2,0,0.4,0,0.7,0c0.8,0,1.6-0.3,1.6-1.2C21,12.3,20.3,12,19.4,12z"/>
                                <path fill="#FFFFFF" d="M20.8,10.3c0-0.7-0.6-1-1.4-1c-0.4,0-0.6,0-0.7,0.1v2h0.7C20.3,11.3,20.8,10.9,20.8,10.3z"/>
                                <path fill="#FFFFFF" d="M36.5,4.5H12.6c-1.1,0-2,0.9-2,2V18c0,1.1,0.9,2,2,2h7.7v8v0.5c-0.9-1.2-2.1-2.2-3.4-2.2
                                    c-0.2,0-0.4,0-0.5,0.1c-0.5,0.1-0.9,0.5-1.2,0.9c-0.3,0.5-0.3,1.1-0.1,1.7c0,0,2.1,5.9,3,8.2c1.1,2.7,2.3,4.1,3.8,4.3
                                    c0.2,0,0.3,0.1,0.5,0.1h10.2c2,0,3.6-1.6,3.6-3.6V26.4c0-1.1-0.9-2.1-2.1-2.1h-0.5c-0.6,0-1.2,0.3-1.6,0.7
                                    c-0.4-0.4-0.9-0.7-1.6-0.7h-0.5c-0.6,0-1.2,0.3-1.6,0.7c-0.4-0.4-0.9-0.7-1.6-0.7H26c-0.4,0-0.8,0.1-1.1,0.3V20h11.6
                                    c1.1,0,2-0.9,2-2V6.5C38.5,5.4,37.6,4.5,36.5,4.5z M24.5,26.9c0.3,0,0.5-0.2,0.5-0.5c0-0.6,0.5-1.1,1.1-1.1h0.5
                                    c0.6,0,1.1,0.5,1.1,1.1c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0-0.6,0.5-1.1,1.1-1.1h0.5c0.6,0,1.1,0.5,1.1,1.1
                                    c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5c0-0.6,0.5-1.1,1.1-1.1H34c0.6,0,1.1,0.5,1.1,1.1v11.8c0,1.4-1.1,2.6-2.6,2.6H22.3
                                    c-0.1,0-0.2,0-0.3,0c-1.2-0.2-2.2-1.4-3.1-3.7c-0.9-2.3-3-8.2-3-8.2c-0.1-0.3-0.1-0.6,0-0.9c0.1-0.2,0.2-0.4,0.5-0.4
                                    c0.1,0,0.2,0,0.3,0c1.2,0,2.7,1.6,3.4,3.2v2.1c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-2.2V28v-8v-3.7c0-0.6,0.5-1.1,1.1-1.1h0.5
                                    c0.6,0,1.1,0.5,1.1,1.1V20v6.3C24,26.6,24.2,26.9,24.5,26.9z M19.2,15.1c-0.6,0-1,0-1.3-0.1V8.8c0.4-0.1,0.9-0.1,1.5-0.1
                                    c0.8,0,1.3,0.1,1.7,0.5c0.3,0.2,0.5,0.6,0.5,1.1c0,0.6-0.4,1.1-1.1,1.4v0c0.6,0.2,1.3,0.6,1.3,1.6c0,0.5-0.2,1-0.5,1.3
                                    C20.9,14.9,20.2,15.1,19.2,15.1z M26.1,15l0-0.7h0c-0.2,0.4-0.7,0.8-1.5,0.8c-0.3-0.4-0.8-0.7-1.3-0.8C23,14,23,13.6,23,13.1v-2.7
                                    h0.8V13c0,0.9,0.3,1.4,1,1.4c0.6,0,0.9-0.4,1.1-0.8c0-0.1,0.1-0.3,0.1-0.4v-2.8h0.8v3.3c0,0.5,0,0.9,0,1.2H26.1z M30.6,13.7
                                    c-0.6,1.6-1,2.4-1.6,2.8c-0.4,0.4-0.8,0.5-1,0.5l-0.2-0.7c0.2-0.1,0.5-0.2,0.7-0.4c0.2-0.2,0.5-0.5,0.7-0.9c0-0.1,0.1-0.2,0.1-0.2
                                    s0-0.1-0.1-0.2l-1.7-4.2h0.9l1,2.7c0.1,0.3,0.2,0.7,0.3,0.9h0c0.1-0.3,0.2-0.6,0.3-0.9l0.9-2.7h0.9L30.6,13.7z"/>
                            </svg>
                            <i>each </i>
                            <b class="counter">3</b>
                        </div>
                        <p>USERS GO TO PRO PLAN</p>
                    </li>
                </ul>
            </div>
        </section>
        <!-- Counter end -->

        <!-- customers -->
        <section id="customers" class="customers version-1">
            <div class="bg-effect image" style="background-image: url('./images/main-bg-1.jpg');">
            </div>
            <div class="container">
                <h2 class="section-title">Our Clients say</h2>
                <p class="section-description">About our work</p>
                <div class="customers-carousel">
                    <div class="caurousel-item">
                        <div class="customers-lazy">
                            <div class="image-block">
                               <img src="images/cust-1-index-1.jpg" alt="" />
                            </div>
                            <div class="description-block">
                                <span class="fa fa-quote-left"></span>
                                <h5>Mary  Forly</h5>
                                <span class="post">Assistant Treasurer</span>
                                <p>All staff was very helpful and we got exactly what we needed, in the timeframe we needed it. I would highly recommend your company to anyone. Profi team is an excellent resource to tackle strategic brand issues. I loved to work with this company.</p>
                            </div>
                        </div>
                    </div>
                    <div class="caurousel-item">
                        <div class="customers-lazy">
                            <div class="image-block">
                                <img src="images/cust-2-index-1.jpg" alt="" />
                            </div>
                            <div class="description-block">
                                <span class="fa fa-quote-left"></span>
                                <h5>Sally Hartford</h5>
                                <span class="post">Project manager</span>
                                <p>We worked together very productively, especially that part of the project, which concerned creativity and generation of ideas. I loved to work with this company. Profi team is an excellent resource to tackle strategic brand issues.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- customers end -->

        <!-- news -->
        <section id="news" class="news-home version-1">
            <h2 class="section-title">Our News</h2>
            <p class="section-description">Don't miss something special!</p>
            <div class="news-body">
                <div class="container">
                    <div class="row">
                        <div class="news-list">
                            <div class="news-item">
                                <div class="col-xs-12 col-sm-6 news-img-block">
                                    <div class="news-date">
                                        <p>09</p>
                                        <span>June</span>
                                    </div>
                                    <img src="images/news-2.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 news-text-block">
                                    <h5>Knowing Your Rights As</h5>
                                    <p class="news-date"><span class="fa fa-calendar"></span>09 June 2015</p>
                                    <p class="line-lg"><span></span></p>
                                    <p class="news-text-block-text">We delve into common stock owners' privileges and how to be vigilant in monitoring a company. Learn how to break down and understand a corporate budget.
                                    </p>
                                    <a class="btn btn-success btn-sm" href="blog_post.html">Read more</a>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="col-xs-12 col-sm-6 news-img-block">
                                    <div class="news-date">
                                        <p>10</p>
                                        <span>June</span>
                                    </div>
                                    <img src="images/news-3.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 news-text-block">
                                    <h5>The Benefits Of Corporate</h5>
                                    <p class="news-date"><span class="fa fa-calendar"></span>10 June 2015</p>
                                    <p class="line-lg"><span></span></p>
                                    <p class="news-text-block-text">Many U.S. companies have found it advantageous to relocate their headquarters rather than face the highest corporate tax rates in the world regardless of whether income was earned domestically ...</p>
                                    <a class="btn btn-success btn-sm" href="blog_post.html">Read more</a>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="col-xs-12 col-sm-6 news-img-block">
                                    <div class="news-date">
                                        <p>11</p>
                                        <span>June</span>
                                    </div>
                                    <img src="images/news-4.jpg" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 news-text-block">
                                    <h5>4 Ways Millennials Can Buy </h5>
                                    <p class="news-date"><span class="fa fa-calendar"></span>11 June 2015</p>
                                    <p class="line-lg"><span></span></p>
                                    <p class="news-text-block-text">Buying private businesses is a good way to have greater control over your investments while increasing your income and avoiding the fluctuations of the market.</p>
                                    <a class="btn btn-success btn-sm" href="blog_post.html">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- news end -->

        <!-- contact-block -->
        <section id="contacts" class="contact-block">
            <div class="bg-effect image" style="background-image: url('./images/contacts-bg.jpg');">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-9">
                        <header>
                            <h2 class="section-title">Contacts us</h2>
                            <p class="section-description">Feel free to contact us</p>
                        </header>
                        <form id="formReservation" autocomplete="on">
                            <div class="row">
                                <div class="col-sm-6 nopadding-right">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-user"></span></div>
                                        <input type="text" name="u_name" placeholder="Name" class="form-control requiredField name" value="" />
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-phone"></span></div>
                                        <input type="text" name="u_phone" placeholder="Phone" class="form-control requiredField phone" value="" />
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="fa fa-at"></span></div>
                                        <input type="email" name="u_mail" placeholder="Enter email" class="form-control requiredField email" value="" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group mail-block">
                                        <div class="input-group-addon"><span class="fa fa-pencil"></span></div>
                                        <textarea class="form-control message" name="u_text" placeholder="Message" rows="4"></textarea>
                                        <input class="btn btn-primary inverse-btn" type="submit" value="Send" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4 col-md-3 nopadding-left">
                        <div class="subscribe">
                            <h2 class="section-title">Subscription</h2>
                            
                            <svg x="0px" y="0px" viewBox="0 0 85.5 103" enable-background="new 0 0 85.5 103" >
                                <path fill="#FFFFFF" d="M52.2,103H2.5c-1.4,0-2.5-1.1-2.5-2.5v-98C0,1.1,1.1,0,2.5,0H83c1.4,0,2.5,1.1,2.5,2.5v67.1
                                c0,0.7-0.3,1.3-0.7,1.8l-30.9,30.9C53.5,102.7,52.8,103,52.2,103z M5,98h46.1l29.4-29.4V5H5V98z"/>
                                <path fill="#FFFFFF" d="M52.2,103c-0.3,0-0.6-0.1-1-0.2c-0.9-0.4-1.5-1.3-1.5-2.3V69.6c0-1.4,1.1-2.5,2.5-2.5H83
                                c1,0,1.9,0.6,2.3,1.5c0.4,0.9,0.2,2-0.5,2.7l-30.9,30.9C53.5,102.7,52.8,103,52.2,103z M54.7,72.1v22.3L77,72.1H54.7z"/>
                                <rect x="11.9" y="13.9" fill="#FFFFFF" width="61.7" height="11.4"/>
                                <rect x="11.9" y="32.4" fill="#FFFFFF" width="29.3" height="57.4"/>
                                <rect x="48.6" y="32.4" fill="#FFFFFF" width="23.9" height="25.9"/>
                                <path fill="#FFFFFF" d="M83,103H2.5c-1.4,0-2.5-1.1-2.5-2.5v-98C0,1.1,1.1,0,2.5,0H83c1.4,0,2.5,1.1,2.5,2.5v98
                                C85.5,101.9,84.4,103,83,103z M5,98h75.5V5H5V98z"/>
                            </svg>

                            <form id="subscribe-form" autocomplete="on" >
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-at"></span>
                                    </div>
                                    <input type="email" name="u_mail" placeholder="Enter email" class="form-control requiredField email" value="" />
                                </div>
                                  <input class="btn btn-primary inverse-btn" type="submit" value="sing up" />
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
        <!-- contact-block end -->
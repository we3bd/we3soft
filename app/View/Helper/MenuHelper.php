<?php

App::uses ( 'AppHelper', 'View/Helper' );

class MenuHelper extends AppHelper {
	
	public $helpers = array ('Html' );
	
	//public menu generator
	public function menuGenerator($threaded, $level = 0, $styleUlclass = null, $styleLiclass = null, $link = 1, $sortable = 1,$lang=null) {
		
		if($lang=='en'){
		 	$val=null;
		 }else{
		 	$val='bn';
		 }
	
		$html = '<ul id="main-menu" class="' . $styleUlclass . '" >';
		foreach ( $threaded as $key => $node ) {
			if ($sortable == 1) {
				$html .= '<div><li>' . '<input type="hidden" value="' . $node ['Menu'] ['id'] . '" name="order[]">';
			} else {
				$html .= '<li >';
			}
			
			foreach ( $node as $type => $threaded ) {
				
				if ($type !== 'children') {
					if ($link == 1) {
						if($threaded['type']== '2'){
	
							if($this->params['language']=='en'){
								if($threaded ['url']=="#"){
									$html .= '<a href="#">' .$threaded ['name'] . '</a>';
								}else{
									$html .= '<a href="' . $this->webroot . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
								}
								
							}else{
								if(empty($this->params['language'])){
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot  . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
									}
									
								}else{
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'.$val] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot . $this->params['language'] . $threaded ['url'] . '">' .$threaded ['name'.$val] . '</a>';
									}
									
								}
								
							}
							
						}else{
							if($this->params['language']=='en'){
								$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
							}else{
								if($this->params['language']){
									$html .= $this->Html->link ( $threaded ['name'.$val], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}else{
									$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}
							}
						}
					} else {
						$html .= $threaded ['name'.$val];
					}
				
				} else {
					if (! empty ( $threaded )) {
						$html .= $this->menuGenerator1 ( $threaded, $level + 2, $styleUlclass, $styleLiclass = null, $link, $sortable,$lang=null );
					}
				}
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	public function menuGenerator5($threaded, $level = 0, $styleUlclass = null, $styleLiclass = null, $link = 1, $sortable = 1,$lang=null) {
		
		if($lang=='en'){
		 	$val=null;
		 }else{
		 	$val='bn';
		 }
	
		$html = '<ul>';
		foreach ( $threaded as $key => $node ) {
			if ($sortable == 1) {
				$html .= '<div><li>' . '<input type="hidden" value="' . $node ['Menu'] ['id'] . '" name="order[]">';
			} else {
				$html .= '<li >';
			}
			
			foreach ( $node as $type => $threaded ) {
				
				if ($type !== 'children') {
					if ($link == 1) {
						if($threaded['type']== '2'){
	
							if($this->params['language']=='en'){
								if($threaded ['url']=="#"){
									$html .= '<a href="#">' .$threaded ['name'] . '</a>';
								}else{
									$html .= '<a href="' . $this->webroot . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
								}
								
							}else{
								if(empty($this->params['language'])){
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot  . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
									}
									
								}else{
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'.$val] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot . $this->params['language'] . $threaded ['url'] . '">' .$threaded ['name'.$val] . '</a>';
									}
									
								}
								
							}
							
						}else{
							if($this->params['language']=='en'){
								$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
							}else{
								if($this->params['language']){
									$html .= $this->Html->link ( $threaded ['name'.$val], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}else{
									$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}
							}
						}
					} else {
						$html .= $threaded ['name'.$val];
					}
				
				} else {
					if (! empty ( $threaded )) {
						$html .= $this->menuGenerator1 ( $threaded, $level + 2, $styleUlclass, $styleLiclass = null, $link, $sortable,$lang=null );
					}
				}
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	public function menuGenerator1($threaded, $level = 0, $styleUlclass = null, $styleLiclass = null, $link = 1, $sortable = 1,$lang=null) {
	
		if($lang=='en'){
			$val=null;
		}else{
			$val='bn';
		}
	
		$html = '<ul >';
		foreach ( $threaded as $key => $node ) {
			if ($sortable == 1) {
				$html .= '<div><li>' . '<input type="hidden" value="' . $node ['Menu'] ['id'] . '" name="order[]">';
			} else {
				$html .= '<li>';
			}
				
			foreach ( $node as $type => $threaded ) {
	
				if ($type !== 'children') {
					if ($link == 1) {
						if($threaded['type']== '2'){
						if($this->params['language']=='en'){
								if($threaded ['url']=="#"){
									$html .= '<a href="#">' .$threaded ['name'] . '</a>';
								}else{
									$html .= '<a href="' . $this->webroot . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
								}
								
							}else{
								if(empty($this->params['language'])){
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot  . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
									}
									
								}else{
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'.$val] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot . $this->params['language'] . $threaded ['url'] . '">' .$threaded ['name'.$val] . '</a>';
									}
									
								}
							}
								
						}else{
							if($this->params['language']=='en'){
								$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
							}else{
								if($this->params['language']){
									$html .= $this->Html->link ( $threaded ['name'.$val], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}else{
									$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}
								
							}
						}
					} else {
						$html .= $threaded ['name'.$val];
					}
	
				} else {
					if (! empty ( $threaded )) {
						$html .= $this->menuGenerator1 ( $threaded, $level + 2, $styleUlclass, $styleLiclass = null, $link, $sortable,$lang=null );
					}
				}
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	
	
	//public menu generator
	public function menuGenerator2($threaded, $level = 0, $styleUlclass = null, $styleLiclass = null, $link = 1, $sortable = 1,$lang=null) {
	
		if($lang=='en'){
			$val=null;
		}else{
			$val='bn';
		}
	
		$html = '<ul id="main-menu" class="' . $styleUlclass . '" >';
		foreach ( $threaded as $key => $node ) {
			if ($sortable == 1) {
				$html .= '<div><li>' . '<input type="hidden" value="' . $node ['Menu'] ['id'] . '" name="order[]">';
			} else {
				$html .= '<li >';
			}
				
			foreach ( $node as $type => $threaded ) {
	
				if ($type !== 'children') {
					if ($link == 1) {
						if($threaded['type']== '2'){
	
							if($this->params['language']=='en'){
								if($threaded ['url']=="#"){
									$html .= '<a href="#">' .$threaded ['name'] . '</a>';
								}else{
									$html .= '<a href="' . $this->webroot . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
								}
	
							}else{
								if(empty($this->params['language'])){
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot  . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
									}
										
								}else{
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'.$val] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot . $this->params['language'] . $threaded ['url'] . '">' .$threaded ['name'.$val] . '</a>';
									}
										
								}
	
							}
								
						}else{
							if($this->params['language']=='en'){
								$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
							}else{
								if($this->params['language']){
									$html .= $this->Html->link ( $threaded ['name'.$val], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}else{
									$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}
							}
						}
					} else {
						$html .= $threaded ['name'.$val];
					}
	
				} else {
					if (! empty ( $threaded )) {
						$html .= $this->menuGenerator4 ( $threaded, $level + 2, $styleUlclass, $styleLiclass = null, $link, $sortable,$lang=null );
					}
				}
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	public function menuGenerator4($threaded, $level = 0, $styleUlclass = null, $styleLiclass = null, $link = 1, $sortable = 1,$lang=null) {
	
		if($lang=='en'){
			$val=null;
		}else{
			$val='bn';
		}
	
		$html = '<ul style="padding-left:30px;">';
		foreach ( $threaded as $key => $node ) {
			if ($sortable == 1) {
				$html .= '<div><li>' . '<input type="hidden" value="' . $node ['Menu'] ['id'] . '" name="order[]">';
			} else {
				$html .= '<li>';
			}
	
			foreach ( $node as $type => $threaded ) {
	
				if ($type !== 'children') {
					if ($link == 1) {
						if($threaded['type']== '2'){
							if($this->params['language']=='en'){
								if($threaded ['url']=="#"){
									$html .= '<a href="#">' .$threaded ['name'] . '</a>';
								}else{
									$html .= '<a href="' . $this->webroot . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
								}
	
							}else{
								if(empty($this->params['language'])){
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot  . 'en' . $threaded ['url'] . '">' .$threaded ['name'] . '</a>';
									}
										
								}else{
									if($threaded ['url']=="#"){
										$html .= '<a href="#">' .$threaded ['name'.$val] . '</a>';
									}else{
										$html .= '<a href="' . $this->webroot . $this->params['language'] . $threaded ['url'] . '">' .$threaded ['name'.$val] . '</a>';
									}
										
								}
							}
	
						}else{
							if($this->params['language']=='en'){
								$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
							}else{
								if($this->params['language']){
									$html .= $this->Html->link ( $threaded ['name'.$val], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}else{
									$html .= $this->Html->link ( $threaded ['name'], array ('controller' => 'pages', 'action' => 'details', $threaded ['slug'] ) );
								}
	
							}
						}
					} else {
						$html .= $threaded ['name'.$val];
					}
	
				} else {
					if (! empty ( $threaded )) {
						$html .= $this->menuGenerator4 ( $threaded, $level + 2, $styleUlclass, $styleLiclass = null, $link, $sortable,$lang=null );
					}
				}
			}
			$html .= '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	

}
	
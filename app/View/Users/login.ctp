<div class="container">
	<div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">

					    <?php echo $this->Session->flash("auth"); ?>
					    
					    <h2><?php __('Login Here'); ?></h2>
					    <?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login')));?>
					        <fieldset>
					        <div class="form-group"><?php echo $this->Form->input('username',array('autofocus'=>'','class'=>'form-control'));?></div>
					        <div class="form-group"><?php echo $this->Form->input('password',array('class'=>'form-control'));?></div>
					        <div class="forget"><?php  echo $this->Html->link('Forgot Password ?', array('controller' => 'users', 'action' => 'fp'));?></div>
					        <?php echo $this->Form->button('login',array('class'=>'btn btn-lg btn-success btn-block'))?>
					        </fieldset>
					    <?php echo $this->Form->end();?>


                    </div>
                </div>
            </div>
        </div>
</div>


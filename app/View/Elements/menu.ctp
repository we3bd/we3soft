 <style>
.x-navigation li > ul li > a {
   
    font-size: 15px !important;
   
}
.x-navigation li > a {

    font-size: 17px !important;
 
}
</style>
<ul id="nav" class="x-navigation x-navigation-open">

	<li class="active">
        <?php 
        echo $this->Html->link ( '<span class="fa fa-dashboard"></span><span class="xn-text">Dashboard</span>', array ('controller'=>'users','action' => 'dashboard'),array('escape'=>false) );
        ?>
    </li>
    <ul>
        	 <li>
		        <?php 
		        	echo $this->Html->link ( 'Member Info', array ('controller'=>'members','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
		        
	       <li>
		        <?php 
		        	echo $this->Html->link ( 'Worker Info', array ('controller'=>'workers','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>

	        <li>
		        <?php 
		        	echo $this->Html->link ( 'Area Info', array ('controller'=>'areas','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	      
		        <li>
		        <?php 
		        	echo $this->Html->link ( 'Balance Info', array ('controller'=>'balences','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	        <li>
		        <?php 
		        	echo $this->Html->link ( 'Loan Info', array ('controller'=>'loans','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	        <li>
		        <?php 
		        	echo $this->Html->link ( 'Balance Kisti', array ('controller'=>'balancekistis','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	        <li>
		        <?php 
		        	echo $this->Html->link ( 'Loan Kisti', array ('controller'=>'lonkistis','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	        <li>
		        <?php 
		        	echo $this->Html->link ( 'User', array ('controller'=>'users','action' => 'index'),array('escape'=>false) );
		        ?>
	        </li>
	        
	        <li class="xn-openable"><a href="#"> <span class="xn-text">Report</span></a>
		<ul>
			<li>
	          	<?php 
		        	echo $this->Html->link ( 'Loan Report', array ('controller'=>'loans','action' => 'loanreport'),array('escape'=>false) );
		        ?>
			</li>
			<li>
				<?php 
		        	echo $this->Html->link ( 'Balance Report', array ('controller'=>'balences','action' => 'balancereport'),array('escape'=>false) );
		        ?>
		   	</li> 
		   	
		   	<li>
				<?php 
		        	echo $this->Html->link ( 'Indivisual Loan Report', array ('controller'=>'loans','action' => 'indloanreport'),array('escape'=>false) );
		        ?>
		   	</li>
		   	
		   	<li>
				<?php 
		        	echo $this->Html->link ( 'Indivisual Balance Report', array ('controller'=>'balences','action' => 'indbalancereport'),array('escape'=>false) );
		        ?>
		   	</li>
		   	
		   	
		</ul>
	</li>
	        
	        
		</ul>

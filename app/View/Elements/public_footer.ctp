<!-- footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 footer-contacts-col">
                        <h4>Contacts</h4>

                        <p class="line-lg"><span></span></p>

                        <p class="footer-adress">35 west 15th street, New York</p>
                        <a class="footer-tell" href="tel%253A+380445555555.html">+38 044 555 55 55</a>
                        <a class="footer-mail" href="mailto:info@profitheme.com">info@profitheme.com</a>

                        <ul class="footer-social">
                            <li><a href="wsdindex.html#"><span class="fa fa-linkedin"></span></a></li>
                            <li><a href="wsdindex.html#"><span class="fa fa-pinterest-p"></span></a></li>
                            <li><a href="wsdindex.html#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="wsdindex.html#"><span class="fa fa-facebook"></span></a></li>
                        </ul>

                        <div class="footer-copyright">
                            <p>&copy; 2015 ProfiTheme.</p>
                            <p>All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 footer-services-col">
                        <h4>SERVICES</h4>
                        <p class="line-lg"><span></span></p>

                        <a class="services-footer" href="wsdindex.html#">Business processes</a>
                        <a class="services-footer" href="wsdindex.html#">Deal services</a>
                        <a class="services-footer" href="wsdindex.html#">Business processes</a>

                        <h5>Instagram</h5>
                        <ul class="instagram-list">
                            <li>
                                <a href="wsdindex.html#">
                                    <img src="images/team-1-index-4.jpg" alt="" />
                                </a>
                            </li>
                            <li>
                                <a href="wsdindex.html#">
                                    <img src="images/team-2-index-4.jpg" alt="" />
                                </a>
                            </li><li>
                                <a href="wsdindex.html#">
                                    <img src="images/team-3-index-4.jpg" alt="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="col-md-5 map-block">
                        <div id="map">
                            
                        </div>
                        <span class="open-map">
                            <svg x="0px" y="0px" viewBox="0 0 67.1 88.7" enable-background="new 0 0 67.1 88.7">
                            <path class="pin-color" d="M59.1,55.3c4.7-6.1,8-13.4,8-21.8C67.1,15,52.1,0,33.6,0C15,0,0,15,0,33.6c0,8.3,3.3,15.7,8,21.8
                                c9.4,12.1,25.5,33.4,25.5,33.4S50.1,66.9,59.1,55.3z"/>
                            <circle fill="#FFFFFF" cx="33.6" cy="33.6" r="17.8"/>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer end -->
        
        <!-- Modal -->
        <div id="reservation" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close">
                        <i></i>
                    </button>
                    <h4 class="modal-title">Contact form</h4>
                </div>
                <div class="modal-body">
                    <form id="formReservation-2" autocomplete="on">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="fa fa-user"></span></div>
                            <input type="text" name="u_name" placeholder="Name" class="form-control requiredField name" value="" />
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="fa fa-phone"></span></div>
                            <input type="text" name="u_phone" placeholder="Phone" class="form-control requiredField phone" value="" />
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="fa fa-at"></span></div>
                            <input type="email" name="u_mail" placeholder="Enter email" class="form-control requiredField email" value="" />
                        </div>
                        <div class="input-group mail-block">
                            <div class="input-group-addon"><span class="fa fa-pencil"></span></div>
                            <textarea class="form-control message" name="u_text" placeholder="Message" rows="4"></textarea>
                        </div>
                        <input class="btn btn-success" type="submit" value="Send message" />
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal End -->
        <div class="covers"></div>
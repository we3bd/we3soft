<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
		public $components = array(
	    'Session',
		
	    'Auth' => array(
	        'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
	        'logoutRedirect' => array(
	            'controller' => '/'
	        ),
	        'authorize' => array('Controller') // Added this line
	    )
	);
	
	
public function isAuthorized($user) {
    // Admin can access every action
   /*  if (isset($user['role']) && $user['role'] === 'admin') {
        return true;
    } */

    // Default deny
    return true;
}

	public function _setErrorLayout() {
	    if ($this->name == 'CakeError') { 
	         //$this->redirect(array('controller'=>'pages','action' => 'error400'));
	    }
	}

    public function beforeFilter() {
    	//echo $_SERVER['HTTP_REFERER'];
    	$this->Auth->allow('index','signup','login', 'fp');
    	$this->set ( 'logged_in', $this->Auth->loggedIn() );
		$this->set ( 'current_user', $this->Auth->user() );
		$this->set ( 'languages', $this->language);
		$this->set ('menu_data', $this->getMenu(1));
		$this->set ('menu_datab', $this->getMenu(2));
		
    }
    

	
public function getMenu($type){
		$this->loadModel('Menu');
		$data = $this ->Menu ->find(
				'threaded',
				array(
						'recursive' =>-1,
						'fields' => array(
								'Menu.id',
								'Menu.parent_id',
								'Menu.name',
								'Menu.slug',
								'Menu.url',
						),
						'conditions'=>array(''),
						'order' => array('Menu.order')
				)
		);
		return $data;
	}
}

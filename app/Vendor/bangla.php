<?php 

class bangla_english{
	 function bangla_number($int)
	{
		$engNumber = array(1,2,3,4,5,6,7,8,9,0,'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		$bangNumber = array('১','২','৩','৪','৫','৬','৭','৮','৯','০','জানুয়ারি','ফেব্রুয়ারি','মার্চ',' এপ্রিল ','মে','জুন','জুলাই','আগস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
		
		$converted = str_replace($engNumber, $bangNumber, $int);
		return $converted;
	}

    function age($birthday){
        list($day, $month, $year) = explode("-", $birthday);
        $year_diff  = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff   = date("d") - $day;
        if ($day_diff < 0 && $month_diff==0) $year_diff--;
        if ($day_diff < 0 && $month_diff < 0) $year_diff--;
        return $year_diff;
    }


}

?>
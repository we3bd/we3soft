<?php
/*
Usage
=====
	set $this->number 
		
	call $this->Convert();
	
This function returns textual representation of your number;
*/

class ConvertToText
{
	var $number;
	var $p_number;
	var $textual;
	var $dos_temp;
	var $temp;
	var $ekok;
	var $doshok;
	var $shotok;
	var $hazar;
	var $lac;
	var $crore;
	var $in_word;
	
	function ConvertToText()
	{
		$this->in_word = array (
								"0" => "Zero",
								"1" => "One",
								"2" => "Two",
								"3" => "Three",
								"4" => "Four",
								"5" => "Five",
								"6" => "Six",
								"7" => "Seven",
								"8" => "Eight",
								"9" => "Nine",
								"10" => "Ten",
								"11" => "Eleven",
								"12" => "Twelve",
								"13" => "Thirteen",
								"14" => "Fourteen",
								"15" => "Fifteen",
								"16" => "Sixteen",
								"17" => "Seventeen",
								"18" => "Eighteen",
								"19" => "Nineteen",
								"20" => "Twenty",
								"30" => "Thirty",
								"40" => "Fourty",
								"50" => "Fifty",
								"60" => "Sixty",
								"70" => "Seventy",
								"80" => "Eighty",
								"90" => "Ninety"
								);

		$this->p_number = 0;
		$this->number = 0;
		$this->textual = "";
		$this->dos_temp = 0;
		$this->temp = 0;
		$this->ekok = 0;
		$this->doshok = 0;
		$this->shotok = 0;
		$this->hazar = 0;
		$this->lac = 0;
		$this->crore = 0;
	}
	
	function Convert()
	{
		$this->p_number = $this->number;
		
		$this->crore = floor($this->number / 10000000);
		$this->number -= ($this->crore * 10000000);
		$this->lac = floor($this->number / 100000);
		$this->number -= ($this->lac * 100000);
		$this->hazar = floor($this->number / 1000);
		$this->number -= ($this->hazar * 1000);
		$this->shotok = floor($this->number / 100);
		$this->number -= ($this->shotok * 100);
		$this->dos_temp = $this->number;
		$this->doshok = floor($this->number / 10);
		$this->ekok = $this->number - ($this->doshok * 10);

		if($this->crore > 0 && $this->crore <= 19)
		{
		    $this->textual .= $this->in_word[$this->crore] . " Crore ";
		}
		elseif($this->crore != 0)
		{
		    $this->temp = $this->crore / 10;
		    $this->crore -= ($this->temp * 10);
		    if($this->crore != 0)
			{
        		$this->textual .= $this->in_word[$this->temp * 10] . " " . $this->in_word[$this->crore] . " Crore ";
			}
			else
			{
        		$this->textual .= $this->in_word[$this->temp * 10] . " Crore ";
		    }
		}

		if($this->lac > 0 && $this->lac <= 19)
		{
		    $this->textual .= $this->in_word[$this->lac] . " Lac ";
		}
		elseif($this->lac != 0)
		{
		    $this->temp = $this->lac / 10;
		    $this->lac -= ($this->temp * 10);
		    if($this->lac != 0)
			{
        		$this->textual .= $this->in_word[$this->temp * 10] . " " . $this->in_word[$this->lac] . " Lac ";
		    }
			else
			{
        		$this->textual .= $this->in_word[$this->temp * 10] . " Lac ";
			}
		}

		if($this->hazar > 0 && $this->hazar <= 19)
		{
		    $this->textual .= $this->in_word[$this->hazar] . " Thousand ";
		}
		elseif($this->hazar != 0)
		{
		    $this->temp = $this->hazar / 10;
		    $this->hazar -= ($this->temp * 10);
		    if($this->hazar != 0)
			{
		        $this->textual .= $this->in_word[$this->temp * 10] . " " . $this->in_word[$this->hazar] . " Thousand ";
			}
			else
			{
	        	$this->textual .= $this->in_word[$this->temp * 10] . " Thousand ";
			}
		}

		if($this->shotok != 0)
		{
		    $this->textual .= $this->in_word[$this->shotok] . " Hundred ";
		}

		if($this->doshok == 1)
		{
		    $this->textual .= $this->in_word[$this->dos_temp];
		    $this->ekok = 0;
		}
		elseif($this->doshok != 0)
		{
		    $this->textual .= $this->in_word[$this->doshok * 10] . " ";
		}

		if($this->ekok != 0)
		{
		    $this->textual .= $this->in_word[$this->ekok];
		}
		elseif($this->p_number == 0)
		{
		    $this->textual .= $this->in_word[$this->ekok];
		}

		return 'Taka ' . $this->textual . ' Only';
	}
}
?>
<?php
App::uses('Thana', 'Model');

/**
 * Thana Test Case
 */
class ThanaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.thana',
		'app.division',
		'app.district',
		'app.student',
		'app.pthana',
		'app.pdistrict',
		'app.pdivision'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Thana = ClassRegistry::init('Thana');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Thana);

		parent::tearDown();
	}

}

<?php
App::uses('Contentimage', 'Model');

/**
 * Contentimage Test Case
 */
class ContentimageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contentimage',
		'app.content'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Contentimage = ClassRegistry::init('Contentimage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contentimage);

		parent::tearDown();
	}

}

<?php
App::uses('Contenttype', 'Model');

/**
 * Contenttype Test Case
 */
class ContenttypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contenttype',
		'app.content',
		'app.perlament',
		'app.contentimage',
		'app.menu'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Contenttype = ClassRegistry::init('Contenttype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contenttype);

		parent::tearDown();
	}

}

<?php
/**
 * Student Fixture
 */
class StudentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'thana_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'district_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'division_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pthana_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pdistrict_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pdivision_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pscRoll' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'jscRoll' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'sscRoll' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'pscReg' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pscgpa' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'jscReg' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'jscgpa' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'jscpassingyear' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pscpassingyear' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sscReg' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'sscgpa' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'sscpassingYear' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'first_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'gender' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'father_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'father_namebn' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 120, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'mother_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'mother_namebn' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 120, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'presentAddress' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'postoffice' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'perpostoffice' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'postofficebn' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'perpostofficebn' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'postcode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'perpostcode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'prasentAddressbn' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'permanentAddres' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'permanentAddressbn' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dob' => array('type' => 'date', 'null' => false, 'default' => null),
		'mobile' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mobileHome' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'birthcertificateNo' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 25, 'unsigned' => false),
		'bloodGroups' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'age' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'fathercell' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'date', 'null' => false, 'default' => null),
		'status' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'thana_id' => 1,
			'district_id' => 1,
			'division_id' => 1,
			'pthana_id' => 1,
			'pdistrict_id' => 1,
			'pdivision_id' => 1,
			'pscRoll' => 1,
			'jscRoll' => 1,
			'sscRoll' => 1,
			'pscReg' => 'Lorem ipsum dolor ',
			'pscgpa' => 1,
			'jscReg' => 'Lorem ipsum dolor ',
			'jscgpa' => 1,
			'jscpassingyear' => 'Lor',
			'pscpassingyear' => 'Lor',
			'sscReg' => 1,
			'sscgpa' => 1,
			'sscpassingYear' => 'Lor',
			'first_name' => 'Lorem ipsum dolor sit amet',
			'gender' => 'Lorem ipsum dolor sit ame',
			'father_name' => 'Lorem ipsum dolor sit amet',
			'father_namebn' => 'Lorem ipsum dolor sit amet',
			'mother_name' => 'Lorem ipsum dolor sit amet',
			'mother_namebn' => 'Lorem ipsum dolor sit amet',
			'presentAddress' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'postoffice' => 'Lorem ipsum dolor sit amet',
			'perpostoffice' => 'Lorem ipsum dolor sit amet',
			'postofficebn' => 'Lorem ipsum dolor sit amet',
			'perpostofficebn' => 'Lorem ipsum dolor sit amet',
			'postcode' => 'Lorem ipsum dolor sit amet',
			'perpostcode' => 'Lorem ipsum dolor sit amet',
			'prasentAddressbn' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'permanentAddres' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'permanentAddressbn' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'dob' => '2016-02-02',
			'mobile' => 'Lorem ipsum dolor sit amet',
			'mobileHome' => 'Lorem ipsum dolor sit amet',
			'birthcertificateNo' => 1,
			'bloodGroups' => 'Lorem ipsum dolor sit ame',
			'age' => 'Lorem ipsum dolor sit amet',
			'fathercell' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-02-02',
			'status' => 1
		),
	);

}

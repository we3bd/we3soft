<?php
/**
 * Contentimage Fixture
 */
class ContentimageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'imgTitlebn' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 150, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'imgTxtbn' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 200, 'unsigned' => false),
		'order' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'type' => array('type' => 'string', 'null' => true, 'default' => '1', 'length' => 1, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'content_id' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'imgTitle' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'imgTxt' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'imgTitlebn' => 'Lorem ipsum dolor sit amet',
			'imgTxtbn' => 1,
			'order' => 1,
			'type' => 'Lorem ipsum dolor sit ame',
			'content_id' => 'Lorem ipsum dolor sit amet',
			'imgTitle' => 'Lorem ipsum dolor sit amet',
			'imgTxt' => 'Lorem ipsum dolor sit amet'
		),
	);

}
